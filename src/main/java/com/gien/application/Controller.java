package com.gien.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PostAuthorize;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.gien.application.jwt.ApplicationUserRepository;

@RestController
public class Controller {
	
	@Autowired
	ApplicationUserRepository userRepository;

	@RequestMapping(value="/servicio", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> inicioNoAuth(){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object myUser = (auth != null) ? auth.getAuthorities() :  null;
		
		System.out.println("servicio: "+myUser.toString());
		
		
		return ResponseEntity.ok("{\"Result\":\"Autorizado\"}");
	}

	@RequestMapping(value="/servicioN1", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	@PostAuthorize("hasRole('ROLE_ADMIN')")
	public ResponseEntity<?> inicioN1(){

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Object myUser = (auth != null) ? auth.getAuthorities() :  null;
		
		System.out.println("servicioN1: "+myUser.toString());
		
		
		return ResponseEntity.ok("{\"Result\":\"Autorizado\"}");
	}
}
