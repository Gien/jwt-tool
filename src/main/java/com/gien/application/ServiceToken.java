package com.gien.application;

import java.nio.charset.Charset;
import java.security.Key;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;

import com.gien.application.jwt.LoginDTO;
import com.gien.application.jwt.ApplicationUserRepository;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;

public class ServiceToken {
	
	public String getToken(String username, String password){
		
		Claims claims = Jwts.claims().setSubject(username);
        //claims.put("scopes", userContext.getAuthorities().stream().map(s -> s.toString()).collect(Collectors.toList()));

        LocalDateTime currentTime = LocalDateTime.now();
        
        String token = Jwts.builder()
          .setClaims(claims)
          .setIssuer(password)
          .setIssuedAt(Date.from(currentTime.atZone(ZoneId.systemDefault()).toInstant()))
          .setExpiration(Date.from(currentTime
              .plusMinutes(123456789)
              .atZone(ZoneId.systemDefault()).toInstant()))
          .signWith(SignatureAlgorithm.HS512, password)
        .compact();
		
		return token;
	}
	
	public String createJWT(String username, String password) {
		 
	    //The JWT signature algorithm we will be using to sign the token
	    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
	 
	    long nowMillis = System.currentTimeMillis();
	    Date now = new Date(nowMillis);
	 
	    //We will sign our JWT with our ApiKey secret
	    byte[] apiKeySecretBytes = password.getBytes(Charset.forName("UTF-8"));
	    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
	 
	    //Let's set the JWT Claims
	    JwtBuilder builder = Jwts.builder().setId(username)
	                                .setIssuedAt(now)
	                                .setSubject("subject")//subject)
	                                .setIssuer("issuer")//issuer)
	                                .signWith(signatureAlgorithm, signingKey);
	 
	    long ttlMillis = 60*5*1000;
		//if it has been specified, let's add the expiration
	    if (ttlMillis  >= 0) {
	    long expMillis = nowMillis + ttlMillis;
	        Date exp = new Date(expMillis);
	        builder.setExpiration(exp);
	    }
	 
	    //Builds the JWT and serializes it to a compact, URL-safe string
	    return builder.compact();
	}
	
	public void parseJWT(String jwt, String password) {
		try {
		    SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
		    byte[] apiKeySecretBytes = password.getBytes(Charset.forName("UTF-8"));
		    Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());
		    
		    //This line will throw an exception if it is not a signed JWS (as expected)
		    Claims claims = Jwts.parser()         
		       .setSigningKey(password.getBytes(Charset.forName("UTF-8")))
		       .parseClaimsJws(jwt).getBody();
		    System.out.println("ID: " + claims.getId());
		    System.out.println("Subject: " + claims.getSubject());
		    System.out.println("Issuer: " + claims.getIssuer());
		    System.out.println("Expiration: " + claims.getExpiration());
		}catch(SignatureException e){
			e.printStackTrace();
		}
	}
}
