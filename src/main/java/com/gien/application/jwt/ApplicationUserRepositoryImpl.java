package com.gien.application.jwt;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Component;

@Component
public class ApplicationUserRepositoryImpl implements ApplicationUserRepository {

	List<LoginDTO> users = new ArrayList<LoginDTO>();
	
	@Override
	public void save(LoginDTO user){
		users.add(user);
	}
	
	@Override
	public LoginDTO find(String username){
		for(int i=0; i <= users.size(); i++){
			if (users.get(i).getUsername().equals(username)){
				return users.get(i);
			}
		}
		
		return null;
	}
	
	
}
