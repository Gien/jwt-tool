package com.gien.application.jwt;

public interface ApplicationUserRepository {

	void save(LoginDTO user);

	LoginDTO find(String username);

}
