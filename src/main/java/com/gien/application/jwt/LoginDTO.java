package com.gien.application.jwt;

public class LoginDTO {
    private String username;
    private String password;
    
    public String getUsername() {
        return username;
    }
    public void setUsername(String username) {
        this.username = username;
    }
    public LoginDTO withUsername(String username){
        this.username = username;
    	return this;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public LoginDTO withPassword(String password){
        this.password = password;
    	return this;
    }
}