package com.gien.application.jwt.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.gien.application.jwt.LoginDTO;
import com.gien.application.jwt.ApplicationUserRepository;

import static java.util.Collections.emptyList;

import java.util.Collection;

import javax.annotation.PostConstruct;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	ApplicationUserRepository userRepository;
	
	@Autowired BCryptPasswordEncoder bCryptPasswordEncoder;
	
	@PostConstruct
	public void init() {
		userRepository.save(new LoginDTO().withUsername("username").withPassword(bCryptPasswordEncoder.encode("admin123")));
		
	}
	
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        LoginDTO applicationUser = userRepository.find(username);
        
        if (applicationUser == null) {
            throw new UsernameNotFoundException(username);
        }
        return new User(applicationUser.getUsername(), applicationUser.getPassword(), (Collection<? extends GrantedAuthority>) emptyList());
    }
}