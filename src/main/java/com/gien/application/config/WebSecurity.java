package com.gien.application.config;

import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.gien.application.jwt.JWTAuthenticationFilter;
import com.gien.application.jwt.JWTAuthorizationFilter;

import static com.gien.application.SecurityConstants.EXPIRATION_TIME;
import static com.gien.application.SecurityConstants.HEADER_STRING;
import static com.gien.application.SecurityConstants.SECRET;
import static com.gien.application.SecurityConstants.SIGN_UP_URL;
import static com.gien.application.SecurityConstants.TOKEN_PREFIX;

@EnableWebSecurity
public class WebSecurity extends WebSecurityConfigurerAdapter {
	
    private UserDetailsService userDetailsService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    
    public WebSecurity(UserDetailsService userDetailsService, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDetailsService = userDetailsService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
        	.csrf()
        	.disable()
        	.authorizeRequests()
//        	.antMatchers("/login").permitAll()
        	.antMatchers(HttpMethod.GET, SIGN_UP_URL).permitAll()
//        	.antMatchers("/servicio").access("hasRole('ROLE_ADMIN')")
            .anyRequest().authenticated()
        .and()
            .addFilter(new JWTAuthenticationFilter(authenticationManager()))
            .addFilter(new JWTAuthorizationFilter(authenticationManager()));
    }
    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
