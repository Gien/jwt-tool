package com.gien.application.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.security.web.csrf.CsrfFilter;

import com.gien.application.jwt.JwtAuthentification;

//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
    
	@Autowired
	private AuthenticationEntryPoint authEntryPoint;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
            .antMatchers(HttpMethod.OPTIONS, "/**")
            .antMatchers("/management/health")
            .antMatchers("/health")
            .antMatchers("/swagger-ui/index.html")
            .antMatchers("/test/**")
            .antMatchers("/h2-console/**");
    }
    
    @Override 
    protected void configure(HttpSecurity http) throws Exception {
        http
            .csrf()
            .disable()
            .headers()
            .frameOptions()
            .disable()
//        .and()
//            .sessionManagement()
//            .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
            .authorizeRequests()
            .antMatchers("/login").permitAll()
            .anyRequest().authenticated()
        .and()
            .httpBasic()
            .authenticationEntryPoint(new JwtAuthentification());
//            .antMatchers("/management/health").permitAll()
//            .antMatchers("/swagger-resources/configuration/ui").permitAll();
//        	
//    	http
//    		.csrf()
//    		.disable()
//    		.authorizeRequests()
//    		.antMatchers("/login").permitAll()
//			.anyRequest()
//			.authenticated()
//		.and()
//			.httpBasic()
//			.authenticationEntryPoint(authEntryPoint);
    	
//        http.exceptionHandling().authenticationEntryPoint(authEntryPoint);
        
//        http.addFilterAfter(new JwtValidationFilter(authenticationManager(), authenticationEntryPoint),
//                CsrfFilter.class);
    }

	@Bean
	public BasicAuthenticationEntryPoint getBasicAuthEntryPoint(){
		BasicAuthenticationEntryPoint basicAuthEntryPoint = new BasicAuthenticationEntryPoint();
		basicAuthEntryPoint.setRealmName("probando");
		return basicAuthEntryPoint;
	}
}
