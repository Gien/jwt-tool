package com.gien.application;

public class SecurityConstants {
    public static final String SECRET = "SecretKeyToGenJWTs";
//    public static final long EXPIRATION_TIME = 864000000; // 10 days
    //mi * ss * mils
    public static final long EXPIRATION_TIME = 5 * 60 *1000; 
    public static final String TOKEN_PREFIX = "Bearer ";
    public static final String HEADER_STRING = "Authorization";
    public static final String SIGN_UP_URL = "/login";
}